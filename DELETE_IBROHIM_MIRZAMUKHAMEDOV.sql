-- Deleting rental records related to the film
--Remove a previously inserted film from the inventory and all corresponding rental records
DELETE FROM "rental"
WHERE "inventory_id" IN (
  SELECT "inventory_id"
  FROM "inventory"
  WHERE "film_id" = (SELECT "film_id" 
				  FROM "film" 
                  WHERE "title" = 'bridge to terabithia'));

-- Deleting film inventory records
DELETE FROM "inventory" 
WHERE "film_id" = (SELECT "film_id" 
				  FROM "film" 
                  WHERE "title" = 'bridge to terabithia');
                
-- Deleting film
--DELETE FROM "film"
--WHERE "title" = 'bridge to terabithia';  
 

-- Deleting customer data
--Remove any records related to you (as a customer) from all tables except "Customer" and "Inventory"
DELETE FROM "payment"
WHERE "customer_id" IN (SELECT "customer_id" 
                      FROM "customer"
                      WHERE "first_name" = 'Ibrohim' AND "last_name" = 'Mirzamukhamedov');

DELETE FROM "rental"
WHERE "customer_id" IN (SELECT "customer_id" 
                      FROM "customer"
                      WHERE "first_name" = 'MukhammadMunir' AND "last_name" = 'Mirzamukhamedov');